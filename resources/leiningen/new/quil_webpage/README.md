# {{name}} 

A Quil sketch designed to ... well, that part is up to you.

## Usage

* `$ lein new quil-webpage <project name>` create a new project with this template.
* `$ lein fighweel` let you serve locally the page containing your sketch and reload it automatically when you modify a source file
* `$ lein clean && lein cljsbuild once release` gives you a version of your sketch ready to be deployed in ./resources/public

## License

Copyright © {{year}} FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
