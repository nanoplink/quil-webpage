# quil-webpage

Quil-webpage is a Leiningen template for the cljs version of Quil. It gives you
a basic html page, wait before the DOM is loaded to start the sketch, and
separate the sketch code from the rest of your project.

It's not much, but i found myself repeating this setup over and over for every
of my sketchs. I hope it would help somebody else than me.

Don't hesitate to open an issue or a merge request !

## Usage

* `$ lein new quil-webpage <project name>` create a new project with this template.
* `$ lein fighweel` let you serve locally the page containing your sketch and reload it automatically when you modify a source file
* `$ lein clean && lein cljsbuild once release` gives you a version of your sketch ready to be deployed in ./resources/public

## License

Copyright © 2017 nanoplink

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
