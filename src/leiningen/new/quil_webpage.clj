(ns leiningen.new.quil-webpage
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files year date multi-segment sanitize-ns]]
            [leiningen.core.main :as main]))

(def render (renderer "quil-webpage"))

(defn quil-webpage
  "Populate files from the template"
  [name]
  (let [data {:name (sanitize-ns name)
              :sanitized (name-to-path name)
              :year (year)
              :date (date)}]
    (main/info "Generating fresh 'lein new' quil-webpage project.")
    (->files data
             ["project.clj" (render "project.clj" data)]
             ["src/{{sanitized}}/core.cljs" (render "core.cljs" data)]
             ["src/{{sanitized}}/sketch.cljs" (render "sketch.cljs" data)]
             ["resources/public/index.html" (render "index.html" data)]
             ["LICENSE" (render "LICENSE" data)]
             ["CHANGELOG.md" (render "CHANGELOG.md" data)]
             ["README.md" (render "README.md" data)]
             [".gitignore" (render ".gitignore" data)]
             "resources")))
