(defproject quil-webpage/lein-template "0.1.3"
  :description "Cljs quil template with figwheel"
  :url "https://gitlab.com/nanoplink/quil-webpage"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :eval-in-leiningen true)
